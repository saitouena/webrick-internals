require 'webrick'

root = File.expand_path '.'
server = WEBrick::HTTPServer.new :Port => 8000, :DocumentRoot => root

trap 'INT' do server.shutdown end # set hook for ^C

server.mount_proc '/' do |req, res|
  res.body = 'Hello, world!'
end

server.start
