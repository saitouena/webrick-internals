# see lib/webrick/httpauth/basicauth.rb,https://docs.ruby-lang.org/ja/latest/class/WEBrick=3a=3aHTTPAuth=3a=3aBasicAuth.html
require 'webrick'
config = { :Realm => 'BasicAuth example realm' }
htpasswd = WEBrick::HTTPAuth::Htpasswd.new 'my_password_file'
htpasswd.set_passwd config[:Realm], 'username', 'password'
config[:UserDB] = htpasswd
basic_authenticator = WEBrick::HTTPAuth::BasicAuth.new config
srv = WEBrick::HTTPServer.new({ :Port => 8000})
srv.mount_proc('/basic_auth') do |req,res|
  basic_authenticator.authenticate(req, res)
  res.body = "authed"
end
srv.start
