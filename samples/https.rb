require 'webrick'
require 'webrick/https'

cert_name = [
  %w[CN localhost],
]
root = File.expand_path '.'
server = WEBrick::HTTPServer.new(:Port => 8000,
                                 :SSLEnable => true,
                                 :SSLCertName => cert_name,
                                 :DocumentRoot => root)

trap 'INT' do server.shutdown end # set hook for ^C

server.start
